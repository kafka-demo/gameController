package com.demo.gamesimulator.gamecontroller.service;


import com.demo.gamesimulator.gamecontroller.config.GameProperties;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@EnableKafkaStreams
@EnableKafka
public class GameStream {
    @Value("${bootstrap.servers}") private String bootStrap;

    private static final Logger log = LoggerFactory.getLogger(GameStream.class);
    private AtomicInteger counter = new AtomicInteger();
    @Autowired
    private GameProperties gameProperties;
    private int totalGameCount;

    @Autowired
    public GameStream(GameProperties properties) {
        this.gameProperties = properties;
        totalGameCount = gameProperties.getGamesPerSeason() * gameProperties.getSeasonsPerSimulation();
    }

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public StreamsConfig kStreamsConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "game-seed-stream");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,  Serdes.Integer().getClass().getName());
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class.getName());
        props.put("bootstrap.servers", bootStrap);
        return new StreamsConfig(props);
    }

    @Bean
    public KStream<String, Integer> kStream(StreamsBuilder kStreamBuilder) {
        KStream<String, Integer> stream = kStreamBuilder.stream("seed");
        stream
                .peek((k,v) -> {sleepCountOrQuit();})
                .to("game-seed");
        return stream;
    }

    public void sleepCountOrQuit() {
        int count = counter.incrementAndGet();
        int sleepTime  = count % gameProperties.getGamesPerSeason()  == 0 ? gameProperties.getIntervalBetweenSeason() : gameProperties.getIntervalBetweenGame();
        log.info("Game number = {}, sleepTime = {}", count, sleepTime);
        if (count >= totalGameCount) {
            log.info("totalGameCount = {}, Quitting...", totalGameCount);
            System.exit(0);
        }
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
