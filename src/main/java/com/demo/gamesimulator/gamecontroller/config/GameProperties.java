package com.demo.gamesimulator.gamecontroller.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "game-control")
public class GameProperties {
    private int gamesPerSeason;
    private int seasonsPerSimulation;
    private int intervalBetweenGame;
    private int intervalBetweenSeason;

    public int getGamesPerSeason() {
        return gamesPerSeason;
    }

    public void setGamesPerSeason(int gamesPerSeason) {
        this.gamesPerSeason = gamesPerSeason;
    }

    public int getSeasonsPerSimulation() {
        return seasonsPerSimulation;
    }

    public void setSeasonsPerSimulation(int seasonsPerSimulation) {
        this.seasonsPerSimulation = seasonsPerSimulation;
    }

    public int getIntervalBetweenGame() {
        return intervalBetweenGame;
    }

    public void setIntervalBetweenGame(int intervalBetweenGame) {
        this.intervalBetweenGame = intervalBetweenGame;
    }

    public int getIntervalBetweenSeason() {
        return intervalBetweenSeason;
    }

    public void setIntervalBetweenSeason(int intervalBetweenSeason) {
        this.intervalBetweenSeason = intervalBetweenSeason;
    }
}
