# Game Controller

This app reads from the `seed` topic and writes to the `game-seed` topic

## Getting Started


### Prerequisites

What things you need to install the software and how to install them

```
1. You would need an instance of kafka(1.1.0 and higher) running.
   Update bootstrap.servers in application.yml to point towards your kafka instance.
2. You would need java8 installed
```

### Installing


```
1. ./gradlew bootRun would start your app.

2. You can also run the app through java -jar /build/libs/game-controler-0.0.1-SNAPSHOT.jar
```

The controller starts posting to the game-stream topic based on the control parameters in
application.yml
```
game-control:
  gamesPerSeason: 100
  seasonsPerSimulation: 2
  intervalBetweenGame: 1000
  intervalBetweenSeason: 10000

```

The app automatically stops after finishing it's simulation run.
## Running the tests

## Built With

* [Spring Docs](https://docs.spring.io/spring-kafka/docs/2.1.5.RELEASE/reference/html/)
* [Kafka Docs](https://docs.confluent.io/current/streams/index.html)
